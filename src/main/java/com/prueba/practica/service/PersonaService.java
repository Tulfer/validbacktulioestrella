package com.prueba.practica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.practica.dao.PersonaDAO;
import com.prueba.practica.model.Persona;

@Service
public class PersonaService {

	@Autowired
	private PersonaDAO personaDAO;
	
	public void guardar(Persona persona) {
		persona.setProcesado(false);
		personaDAO.save(persona);
	}
	
	public List<Persona> consultar(){
		return personaDAO.findAll();
	}
	
	public void procesar(List<Persona> personas) {
		personas.forEach((p)->{
			Persona persona = new Persona();
			persona = personaDAO.findById(p.getId()).get();
			persona.setProcesado(true);
			personaDAO.save(persona);
		});
	}
	
}
