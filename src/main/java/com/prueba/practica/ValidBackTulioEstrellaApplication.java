package com.prueba.practica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidBackTulioEstrellaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidBackTulioEstrellaApplication.class, args);
	}

}
