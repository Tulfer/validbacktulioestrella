package com.prueba.practica.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.practica.model.Persona;
import com.prueba.practica.service.PersonaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("personas")
public class PersonaRest {

	@Autowired
	private PersonaService personaService;
	
	@PostMapping("/guardar")
	public void guardar(@RequestBody Persona persona) {
		personaService.guardar(persona);
	}
	
	@GetMapping("/consultar")
	public List<Persona> consultar(){
		return personaService.consultar();
	}
	
	@PutMapping("/procesar")
	public void procesar(@RequestBody List<Persona> personas) {
		personaService.procesar(personas);
	}
	
}
